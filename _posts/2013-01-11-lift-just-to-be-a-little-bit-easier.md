---
layout: post
title:  "Life just need to a little be easier, not a lot"
date:   2013-01-11 21:57:24
categories: thinking
---

# Life just need to a little be easier, not a lot
We are living in an era that's so many people want to make the world much better. There are dozens of new services, web sites or mobile apps created once a while, some of them are pretty useful, some of them not, but you have to acknowledge that they all want to make the world better.

People always love easy things, also we often lean to use the things we familiar with, the best example is EMAIL.I learned that a lot of my friends still using email to save articles they read, todos they need to finish etc. Cause it's way too easy to use that instead of all kinds of apps. This trait -- EASY can beat a lot of fancy apps or web sites. Also it will last longer time than any others.

Another example is plain text, I love the beautify of plain text.Sometimes you may feel it's too easy to record or present want you want to express, but you can not denied it's power to communicate with varies of client softwares.

Sometimes you only need to make the simple tool a little bit better, not reinvent a whole new thing let people adapt.I am not here to praise [Basecamp Breeze](http://basecamp.com/breeze), but it actually just make the email or email list a little bit better, not recreate another group corporation tools, that will be much easier for people to adapt this "new" tool than using a brand new service.

People will feel safe, happy even if it just make our life a little bit easier and better.
